# base image
FROM python:3
LABEL mainteiner="krylova.maria.2629@gmail.com"
# change workdir
WORKDIR /src/
ADD ./src/contacts.txt ./
ADD ./src/*.py ./
# run the application
CMD ["python", "phonebook.py"]


