import pytest
from Contact import is_valid_name
from Contact import is_valid_number
from Contact import Contact
from Book import Phonebook


class TestContact:
    def test_name_valid1(self):
        assert is_valid_name("ivan") is True

    def test_name_valid2(self):
        assert is_valid_name("IvaNN 123") is True

    def test_name_invalid1(self):
        assert is_valid_name("Ванюша") is False

    def test_number_valid1(self):
        assert is_valid_number("89000000000") is True

    def test_number_valid2(self):
        assert is_valid_number("+79000001212") is True

    def test_number_invalid1(self):
        assert is_valid_number("10000000000") is False

    def test_number_invalid2(self):
        assert is_valid_number("asd") is False

    def test_number_invalid3(self):
        assert is_valid_number("812345678900") is False

    def test_contact_init(self):
        contact = Contact(["mrylova", "maria", "89000000000", "01.01.2000"])
        assert contact.name == "Maria"

    def test_contact_date1(self):
        contact = Contact(["mrylova", "maria", "89000000000", "29.02.2001"])
        assert contact.date_of_birth == ""

    def test_contact_date(self):
        contact = Contact(["mrylova", "maria", "89000000000", "28/02/2001"])
        assert contact.date_of_birth == ""

    def test_contact_age(self):
        contact = Contact(["mrylova", "maria", "89000000000", "01.01.2000"])
        assert contact.age() == 20

    def test_phonebook_find1(self):
        phonebook = Phonebook()
        phonebook.add_new_contact("Ivan", "Ivanov",
                                  "81110001100",
                                  "01.01.2000")
        vanya = phonebook.find("Ivan", "Ivanov")
        assert vanya.name == "Ivan"

    def test_phonebook_find2(self):
        phonebook = Phonebook()
        phonebook.add_new_contact("Ivan",
                                  "Ivanov",
                                  "81110001100",
                                  "01.01.2000")
        assert phonebook.find("Vanya", "Ivanov") is None
